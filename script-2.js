
/************************************************************************* */

const input = document.createElement("input");
const addBtn = document.createElement("button");
addBtn.textContent = "Создать";
const ul = document.createElement("ul");

document.body.appendChild(input);
document.body.appendChild(addBtn);
document.body.appendChild(ul);

const taskList = [];

addBtn.addEventListener("click", function() {
  taskList.push({ text: input.value, done: false });
  drawList();
  input.value = ""; //очистить штзге
  console.log(taskList);
});

let oldUl = ul;

drawList();
function drawList() {
  const newUl = document.createElement("ul");
  for (let i = 0; i < taskList.length; i++) {
    newUl.appendChild(drawLi(taskList[i]));
  }
  document.body.replaceChild(newUl, oldUl);
  oldUl = newUl;
}

function drawLi(task) {
  const li = document.createElement("li");
  const span = document.createElement("span");
  span.textContent = task.text;

  const checkBtn = document.createElement("button");
  if (task.done) {
    checkBtn.textContent = "вернуть";
    span.style.textDecoration = "line-through";
  } else {
    checkBtn.textContent = "удалить";
    span.style.textDecoration = "none";
  }

  checkBtn.addEventListener("click", function() {
    task.done = !task.done;
    drawList();
  });

  li.appendChild(span);
  li.appendChild(checkBtn);
  return li;
}

/************************************************************************ */

